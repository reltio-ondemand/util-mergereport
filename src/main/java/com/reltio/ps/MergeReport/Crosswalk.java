package com.reltio.ps.MergeReport;

import java.util.List;

/**
 * 
 * @author Ganesh.Palanisamy@reltio.com Created : Sep 19, 2014
 */
public class Crosswalk {

	public String uri;
	public String type;
	public String value;
	public String reltioLoadDate;
	public String createDate;
	public String updateDate;
	public List<String> attributes;
	public String sourceTable;
	
	public String ownerType;

}
