package com.reltio.ps.MergeReport;

public class ConsistencyCounter {

	Integer total;
	Integer processed;
	
	public ConsistencyCounter() {
		processed = 0;
	}
	
	public void setTotal(Integer i) {
		total = i;
	}
	
	public synchronized void incrementCounter() {
		processed += 1;
		if (processed % 100 == 0) {
			System.out.println("Processed uris: " + processed);
		}
	}
	
	public Boolean isComplete() {
		
		Boolean isSuccess = false;
		
		if (processed * 1.001 > total && processed * 0.999 < total) {
			isSuccess = true;
		}
		
		return isSuccess;
	}
	
	public Integer getTotal() {
		return total;
	}
	
}
