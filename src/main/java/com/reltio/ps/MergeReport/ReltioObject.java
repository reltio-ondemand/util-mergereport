package com.reltio.ps.MergeReport;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 
 * @author Mohan
 */
public class ReltioObject {

	public String type;
	public String uri;
	public String label;
	
	public Map<String, List<Object>> attributes = new HashMap<String, List<Object>>();
	
	public List<Crosswalk> crosswalks;

}
