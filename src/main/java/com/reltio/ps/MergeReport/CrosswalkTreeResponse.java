package com.reltio.ps.MergeReport;

import java.util.ArrayList;
import java.util.List;

class Merges{
	List<Losers>losers;
	//List<Merges> merges; 
	String time;
	String mergeReason;
	String mergeRules;
	String user; // 20170116 - Requirement to show user who created merge

	public List<Merges> getMerges() {
		List<Merges> l_merges = new ArrayList<Merges>();
		l_merges.add(this);
		if (losers != null) {
			for (Losers loser : losers) {
				List<Merges> loserMerges = loser.merges;
				if (loserMerges != null) {
					for (Merges loserMerge :loserMerges) {
						l_merges.addAll(loserMerge.getMerges());
					}
				}
			}
		}
		return l_merges;
	}
}

class Losers{
	List<Crosswalk>crosswalks;
	String uri;
	
	List<Merges> merges;
	
}
