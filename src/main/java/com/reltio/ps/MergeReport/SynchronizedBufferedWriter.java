package com.reltio.ps.MergeReport;

import java.io.BufferedWriter;
import java.io.IOException;

public class SynchronizedBufferedWriter {

	BufferedWriter br;
	
	public SynchronizedBufferedWriter(BufferedWriter buffWriter) {
		br = buffWriter;
	}
	
	public synchronized void write(String str) throws IOException {
		br.write(str);
	}
	
	public synchronized void newLine() throws IOException {
		br.newLine();
	}
	
	public synchronized void flush() throws IOException {
		br.flush();
	}
	
	public synchronized void close() throws IOException {
		br.close();
	}
	
	

}
