package com.reltio.ps.MergeReport;

import java.util.List;

import com.reltio.cst.domain.Attribute;

public class ScanResponseURI {
	private Attribute cursor;

	private List<Uri> objects;

	public Attribute getCursor() {
		return cursor;
	}

	public void setCursor(Attribute cursor) {
		this.cursor = cursor;
	}

	public List<Uri> getObjects() {
		return objects;
	}

	public void setObjects(List<Uri> objects) {
		this.objects = objects;
	}

}
